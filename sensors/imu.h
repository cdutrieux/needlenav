#ifndef SENSORS_IMU
#define SENSORS_IMU

#include "mysettings.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
//#include <EEPROM.h>

class Imu {

    private :
        Adafruit_BNO055 bno;

        void readCalibrationData();

    public: 
        Imu(uint16_t id, unsigned char adress);

        void setup(void);

        void loop(void);

        Adafruit_BNO055 getAFimu();

};

#endif

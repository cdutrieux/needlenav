#ifndef SENSORS_GPS
#define SENSORS_GPS
#include <Adafruit_GPS.h>
//#include <SoftwareSerial.h>

class Gps {
    private:
        HardwareSerial &mySerial;
        Adafruit_GPS AFgps;
        uint32_t timer;
    public:
        Gps(HardwareSerial &mySerial);

        void setup();
        void loop();
        Adafruit_GPS getAFgps();
};
#endif

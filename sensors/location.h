#ifndef SENSORS_LOCATION
#define SENSORS_LOCATION

#include "imu.h"
#include "gps.h"

class Location {
    private:
        Imu _imu = Imu(55, 0x28);
        Gps _gps = Gps(Serial1);
    public:
        Location();

        void setup();

        void loop();

        Gps getGps();

        Imu getImu();
};

#endif
